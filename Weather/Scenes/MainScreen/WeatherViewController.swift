import UIKit
import CoreLocation

class WeatherViewController: UIViewController {
    @IBOutlet weak var localityLabel: UILabel!
    @IBOutlet weak var conditionLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var minusLabel: UILabel!
    @IBOutlet weak var temperaturesLabel: UILabel!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var temperaturesView: UIView!
    @IBOutlet weak var hoursViewTop: NSLayoutConstraint!
    @IBOutlet weak var hoursCollectionView: UICollectionView!
    
    let locationManager = CLLocationManager()
    let weatherViewModel = WeatherViewModel()
    
    var weather: Weather? {
        didSet {
            updateScreen(weather: weather)
        }
    }
        
    var firstTopConstraint = CGFloat.zero
    
    override func viewDidLoad() {
        super.viewDidLoad()
        weatherViewModel.delegate = self
        weatherViewModel.loadWeatherFromCache()
        registerNibs()
        getUserLocationWeather()
        addBackgroundView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        firstTopConstraint = hoursViewTop.constant
        mainTableView.allowsSelection = false
    }
    
    private func registerNibs() {
        let hourCollectionCell = UINib(nibName: "HourCollectionViewCell", bundle: nil)
        hoursCollectionView.register(hourCollectionCell, forCellWithReuseIdentifier: "HourCell")
        let dayTableViewCell = UINib(nibName: "DayTableViewCell", bundle: nil)
        mainTableView.register(dayTableViewCell, forCellReuseIdentifier: "DayCell")
        let infoTableViewCell = UINib(nibName: "InfoTableViewCell", bundle: nil)
        mainTableView.register(infoTableViewCell, forCellReuseIdentifier: "InfoTableViewCell")
        let extraInfoTableViewCell = UINib(nibName: "ExtraInfoTableViewCell", bundle: nil)
        mainTableView.register(extraInfoTableViewCell, forCellReuseIdentifier: "ExtraInfoTableViewCell")
    }
    
    private func addBackgroundView() {
        let backImage = UIImage(named: "blueBack")
        let backImageView = UIImageView(image: backImage)
        backImageView.frame = CGRect(x: 0, y: 0,
                                     width: UIScreen.main.bounds.width,
                                     height: UIScreen.main.bounds.height)
        self.view.addSubview(backImageView)
        self.view.sendSubviewToBack(backImageView)
    }

    internal func getUserLocationWeather() {
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    private func updateScreen(weather: Weather?) {
        guard let weather = weather else {return}
        localityLabel.text = weather.geo_object.locality.name
        localityLabel.font = localityLabel.font.withSize(40)
        conditionLabel.text = conditionDictionary[weather.fact.condition]
        conditionLabel.font = conditionLabel.font.withSize(20)
        tempLabel.text = "\(weather.fact.temp.magnitude.description)"
        minusLabel.isHidden = weather.fact.temp >= 0
        let tempMin = weather.forecasts.first!.parts.day_short.temp_min
        var tempMax: Int? = 0
        if (0...5).contains(Date().getHour()) {
            tempMax = weather.forecasts[0].parts.night.temp_max
        } else if (6...11).contains(Date().getHour()) {
            tempMax = weather.forecasts[0].parts.morning.temp_max
        } else if (12...17).contains(Date().getHour()) {
            tempMax = weather.forecasts[0].parts.day.temp_max
        } else if (18...23).contains(Date().getHour()) {
            tempMax = weather.forecasts[0].parts.evening.temp_max
        }
        temperaturesLabel.text = "Макс. \(tempMax ?? 0)°, мин. \(tempMin)°"
        temperaturesLabel.font = temperaturesLabel.font.withSize(20)
        mainTableView.reloadData()
        hoursCollectionView.reloadData()
    }
    
    @IBAction private func openSearchScreen() {
        guard let searchVC = storyboard?.instantiateViewController(identifier: "SearchViewController") as? SearchViewController else {return}
        searchVC.delegate = self
        self.present(searchVC, animated: true, completion: nil)
    }
}

extension WeatherViewController: SearchViewControllerDelegate {
    func getUserLocationPlace() {
        getUserLocationWeather()
    }
    
    func searchAnotherPlace(coordinates: GeoLocate) {
        weatherViewModel.downloadWeatherData(latitude: coordinates.lat, longitude: coordinates.lng)
    }
}

extension WeatherViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue = manager.location?.coordinate else { return }
        weatherViewModel.downloadWeatherData(latitude: locValue.latitude, longitude: locValue.longitude)
        locationManager.stopUpdatingLocation()
    }
}

extension WeatherViewController: WeatherViewModelDelegate {
    func updateWeatherObject(weather: Weather) {
        self.weather = weather
    }
}

extension WeatherViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let separator = UIView()
        separator.backgroundColor = UIColor(white: 1, alpha: 0.5)
        separator.frame = CGRect(x: 0, y: 0,
                                 width: tableView.frame.width,
                                 height: 1)
        return separator
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            guard let forecasts = weather?.forecasts else {return 0}
            return forecasts.count - 1
        } else if section == 1 {
            return 1
        } else if section == 2 {
            return 9
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       if indexPath.section == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "DayCell", for: indexPath) as? DayTableViewCell {
                var forecasts = weather?.forecasts
                forecasts?.removeFirst()
                cell.configureView(forecast: forecasts?[indexPath.row])
                return cell
            }
        } else if indexPath.section == 1 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell", for: indexPath) as? InfoTableViewCell {
                cell.configureView(weather: weather)
                return cell
            }
        } else if indexPath.section == 2 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ExtraInfoTableViewCell", for: indexPath) as? ExtraInfoTableViewCell {
                cell.configureView(weather: weather, index: indexPath.row)
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y
        let midDistance = (firstTopConstraint - temperaturesView.frame.height) / 2
        if contentOffset > 0 && hoursViewTop.constant > -(temperaturesView.frame.height) {
            let newAlpha = temperaturesView.alpha - (2 * contentOffset / (hoursViewTop.constant + temperaturesView.frame.height))
            temperaturesView.alpha = newAlpha < 0 ? 0 : newAlpha
            hoursViewTop.constant -= contentOffset
            scrollView.contentOffset.y -= contentOffset
        } else if contentOffset < 0 && hoursViewTop.constant < firstTopConstraint {
            if hoursViewTop.constant > midDistance {
                let newAlpha = temperaturesView.alpha - (contentOffset / temperaturesView.frame.height)
                temperaturesView.alpha = newAlpha > 1 ? 1 : newAlpha
            }
            hoursViewTop.constant -= contentOffset
        }
        if hoursViewTop.constant - contentOffset <= -(temperaturesView.frame.height) {
            hoursViewTop.constant = -(temperaturesView.frame.height)
            scrollView.contentOffset.y -= 0
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let actualPosition = scrollView.panGestureRecognizer.translation(in: scrollView.superview)
        let centerOfHidenView = (-(self.temperaturesView.frame.height) + firstTopConstraint) / 2
        if actualPosition.y < 0 {
            let duration = hoursViewTop.constant < centerOfHidenView ? 0.3 : 1
            UIView.animate(withDuration: duration) {
                self.hoursViewTop.constant = -(self.temperaturesView.frame.height)
                self.view.layoutIfNeeded()
                self.temperaturesView.alpha = 0
            }
        } else if actualPosition.y > 0 {
            if hoursViewTop.constant + 10 >= firstTopConstraint {
                UIView.animate(withDuration: 0.3) {
                    self.hoursViewTop.constant = self.firstTopConstraint
                    self.view.layoutIfNeeded()
                    self.temperaturesView.alpha = 1
                }
            } else {
                UIView.animate(withDuration: 1) {
                    self.hoursViewTop.constant = -(self.temperaturesView.frame.height)
                    self.view.layoutIfNeeded()
                    self.temperaturesView.alpha = 0
                }
            }
        }
    }
}

extension WeatherViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 27
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HourCell", for: indexPath) as? HourCollectionViewCell {
            cell.configureCell(weather: weather, index: indexPath.row, currentHour: Date().getHour())
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HourCell", for: indexPath) as? HourCollectionViewCell {
            cell.configureCell(weather: weather, index: indexPath.row, currentHour: Date().getHour())
            let fittingSize = CGSize(width: UIScreen.main.bounds.width, height: hoursCollectionView.frame.height)
            let size = cell.systemLayoutSizeFitting(fittingSize, withHorizontalFittingPriority: .fittingSizeLevel, verticalFittingPriority: UILayoutPriority(1000))
            return size
        }
        return CGSize()
    }
}

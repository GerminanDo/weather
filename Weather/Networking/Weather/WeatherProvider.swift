import Foundation

class WeatherProvider {
    
    private let baseWeatherUrl = "https://api.weather.yandex.ru/v2/"
    private let APIKey = "18a387c6-2a55-47b1-9371-c7ba3263ee04"
        
    func downloadWeather(latitude: Double, longitude: Double,
                         completion: @escaping (Result<Weather, Error>) -> ()) {
        getWeatherData(latitude: latitude, longitude: longitude) { (data, response, error) in
            do {
                if let data = data {
                    let weather = try JSONDecoder().decode(Weather.self, from: data)
                    DispatchQueue.main.async {
                        completion(.success(weather))
                    }
                }
            } catch {
                completion(.failure(error))
            }
        }
    }
    
    func getWeatherData(latitude: Double, longitude: Double, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        guard let url = URL(string: baseWeatherUrl) else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.setValue(APIKey, forHTTPHeaderField: "X-Yandex-API-Key")
        request.url = URL(string: baseWeatherUrl + "forecast?lat=\(latitude)&lon=\(longitude)&extra=true")
        URLSession.shared.dataTask(with: request, completionHandler: completion).resume()
    }    
}

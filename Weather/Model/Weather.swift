import Foundation

struct Weather: Codable {
    var geo_object: GeoObject
    var fact: Fact
    var forecasts: [Forecast]
}

struct GeoObject: Codable {
    var locality: Locality
}

struct Locality: Codable {
    var name: String
}

struct Fact: Codable {
    var temp: Int
    var feels_like: Int
    var icon: String
    var condition: String
    var prec_strength: Double
    var wind_speed: Double
    var wind_dir: String
    var pressure_pa: Int
    var humidity: Int
    var uv_index: Int
}

struct Forecast: Codable {
    var date: String
    var sunrise: String
    var sunset: String
    var parts: Parts
    var hours: [Hour]
}

struct Parts: Codable {
    var night: DayPartInfo
    var morning: DayPartInfo
    var day: DayPartInfo
    var day_short: DayPartInfo
    var evening: DayPartInfo
}

struct DayPartInfo: Codable {
    var temp_min: Int = 0
    var temp_max: Int? = 0
    var temp: Int? = 0
    var prec_strength: Double = 0
    var icon: String = ""
    var prec_mm: Double = 0
    var prec_prob: Int = 0
}

struct Hour: Codable {
    var hour: String
    var icon: String
    var temp: Int
    var prec_strength: Double
}


import UIKit

class ExtraInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    func configureView(weather: Weather?, index: Int) {
        guard let weather = weather else {return}
        descriptionLabel.text = infoDictionary[index]
        switch index {
        case 0:
            infoLabel.text = weather.forecasts.first!.sunrise
        case 1:
            infoLabel.text = weather.forecasts.first!.sunset
        case 2:
            infoLabel.text = "\(weather.forecasts.first!.parts.day_short.prec_prob.description) %"
        case 3 :
            infoLabel.text = "\(weather.fact.humidity.description) %"
        case 4:
            if let direction = windDictionary[weather.fact.wind_dir] {
                let speed = weather.fact.wind_speed
                let speedStr = speed.truncatingRemainder(dividingBy: 1) > 0 ? speed.description : Int(speed).description
                infoLabel.text = "\(direction) \(speedStr) км/ч"
            }
        case 5:
            infoLabel.text = "\(weather.fact.feels_like.description)°"
        case 6:
            let precMm = weather.forecasts.first!.parts.day_short.prec_mm
            let precMmStr = precMm.truncatingRemainder(dividingBy: 1) > 0 ? precMm.description : Int(precMm).description
            infoLabel.text = "\(precMmStr) мм"
        case 7:
            infoLabel.text = "\(weather.fact.pressure_pa.description) гПа "
        case 8:
            infoLabel.text = weather.fact.uv_index.description
        default:
            return
        }
    }
    
}

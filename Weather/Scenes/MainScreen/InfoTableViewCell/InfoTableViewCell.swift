//
//  InfoTableViewCell.swift
//  Weather
//
//  Created by Anton on 1/17/21.
//

import UIKit

class InfoTableViewCell: UITableViewCell {
    @IBOutlet weak var infoLabel: UILabel!
    
    func configureView(weather: Weather?) {
        infoLabel.text = configureInfo(weather: weather)
    }
    
    private func configureInfo(weather: Weather?) -> String {
        guard let weather = weather else {return ""}
        let condition = weather.fact.condition
        let temp = weather.fact.temp
        let tempMin = weather.forecasts.first!.parts.day_short.temp_min
        var firstStr = "Сегодня: "
        if let condition = conditionDictionary[condition] {
            firstStr += "Сейчас \(condition). "
        }
        firstStr += "Температура воздуха \(temp)°, ожидаемая минимальная температура воздуха сегодня \(tempMin)°."
        return firstStr
    }
    
}

import Foundation

struct Cities: Codable {
    var hits: [City]
}

struct City: Codable {
    var country: Country
    var administrative: [String]
    var locale_names: Locale
    var _geoloc: GeoLocate
    
}

struct Country: Codable {
    var `default`: String
}

struct Locale: Codable {
    var `default`: [String]
}

struct GeoLocate: Codable {
    var lat: Double
    var lng: Double
}

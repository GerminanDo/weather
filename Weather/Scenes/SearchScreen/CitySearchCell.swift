import UIKit

class CitySearchCell: UITableViewCell {
    @IBOutlet weak var label: UILabel!
    
    func configure(text: String) {
        label.text = text
    }
}

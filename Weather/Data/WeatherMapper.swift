import Foundation

class WeatherMapper {
    func map(_ dictionary: [String: Any]) -> Weather? {
        guard let geo = dictionary["geo_object"] as? [String : Any],
              let locality = geo["locality"] as? [String : Any],
              let locName = locality["name"] as? String else { return nil }
        let geoObject = GeoObject(locality: Locality(name: locName))

        guard let fact = dictionary["fact"] as? [String : Any],
              let temp = fact["temp"] as? Int, let feelsLike = fact["feels_like"] as? Int,
              let icon = fact["icon"] as? String, let condition = fact["condition"] as? String,
              let precStrength = fact["prec_strength"] as? Double, let windSpeed = fact["wind_speed"] as? Double,
              let windDir = fact["wind_dir"] as? String, let pressurePa = fact["pressure_pa"] as? Int,
              let humidity = fact["humidity"] as? Int, let uvIndex = fact["uv_index"] as? Int else { return nil }
        let factObject = Fact(temp: temp, feels_like: feelsLike, icon: icon, condition: condition, prec_strength: precStrength, wind_speed: windSpeed, wind_dir: windDir, pressure_pa: pressurePa, humidity: humidity, uv_index: uvIndex)
        
        guard let forecasts = dictionary["forecasts"] as? [[String : Any]] else { return nil }
        
        var forecastsArray: [Forecast] = []
        forecasts.forEach { forecast in
            guard let date = forecast["date"] as? String, let sunrise = forecast["sunrise"] as? String,
            let sunset = forecast["sunset"] as? String,
            let parts = forecast["parts"] as? [String : Any],
            let night = parts["night"] as? [String : Any],
            let morning = parts["morning"] as? [String : Any],
            let day = parts["day"] as? [String : Any],
            let dayShort = parts["day_short"] as? [String : Any],
            let evening = parts["evening"] as? [String : Any],
            let tempMinNight = night["temp_min"] as? Int, let tempMaxNight = night["temp_max"] as? Int,
            let precStrengthNight = night["prec_strength"] as? Double,
            let tempMinMorning = morning["temp_min"] as? Int, let tempMaxMorning = morning["temp_max"] as? Int,
            let precStrengthMorning = morning["prec_strength"] as? Double,
            let tempMinDay = day["temp_min"] as? Int, let tempMaxDay = day["temp_max"] as? Int,
            let precStrengthDay = day["prec_strength"] as? Double,
            let tempMinDayShort = dayShort["temp_min"] as? Int, let tempDayShort = dayShort["temp"] as? Int,
            let iconDayShort = dayShort["icon"] as? String, let precStrengthDayShort = dayShort["prec_strength"] as? Double, let precMm = dayShort["prec_mm"] as? Double, let precProb = dayShort["prec_prob"] as? Int,
            let tempMinEvening = evening["temp_min"] as? Int, let tempMaxEvening = evening["temp_max"] as? Int,
            let precStrengthEvening = evening["prec_strength"] as? Double,
            let hours = forecast["hours"] as? [[String : Any]] else {return}
            
            var hoursArray: [Hour] = []
            hours.forEach { hour in
                guard let time = hour["hour"] as? String, let icon = hour["icon"] as? String,
                      let temp = hour["temp"] as? Int, let precStrength = hour["prec_strength"] as? Double else {return}
                let hourObject = Hour(hour: time, icon: icon, temp: temp, prec_strength: precStrength)
                hoursArray.append(hourObject)
            }
            
            let partsObject = Parts(night: DayPartInfo(temp_min: tempMinNight, temp_max: tempMaxNight, prec_strength: precStrengthNight),
                              morning: DayPartInfo(temp_min: tempMinMorning, temp_max: tempMaxMorning, prec_strength: precStrengthMorning),
                              day: DayPartInfo(temp_min: tempMinDay, temp_max: tempMaxDay, prec_strength: precStrengthDay),
                              day_short: DayPartInfo(temp_min: tempMinDayShort, temp: tempDayShort, prec_strength: precStrengthDayShort, icon: iconDayShort, prec_mm: precMm, prec_prob: precProb),
                              evening: DayPartInfo(temp_min: tempMinEvening, temp_max: tempMaxEvening, prec_strength: precStrengthEvening))
            
            let forecastObject = Forecast(date: date, sunrise: sunrise, sunset: sunset, parts: partsObject, hours: hoursArray)
            forecastsArray.append(forecastObject)
        }
        return Weather(geo_object: geoObject, fact: factObject, forecasts: forecastsArray)
    }
}

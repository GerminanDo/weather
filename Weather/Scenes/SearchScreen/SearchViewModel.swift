import Foundation

protocol SearchViewModelDelegate: class {
    func updateTable(with cities: Cities)
}

class SearchViewModel {
    
    private let manager = NetworkingManager.cities
    
    weak var delegate: SearchViewModelDelegate?
    
    func getCities(query: String) {
        manager.getCities(query: query) { [weak self] result in
            switch result {
            case .success(let cities):
                self?.delegate?.updateTable(with: cities)
            case .failure:
                break
            }
        }
    }
}

import Foundation

protocol WeatherViewModelDelegate {
    func updateWeatherObject(weather: Weather)
}

class WeatherViewModel {
    
    weak var delegate: WeatherViewController?
    
    func downloadWeatherData(latitude: Double, longitude: Double) {
        NetworkingManager.weather.downloadWeatherData(latitude: latitude, longitude: longitude, completion: { [weak self] result in
            switch result {
            case .success(let weather):
                DataManager.weather.cacheWeather(data: weather)
                self?.delegate?.updateWeatherObject(weather: weather)
            case .failure:
                break
            }
        })
    }
    
    func loadWeatherFromCache() {
        DataManager.weather.loadWeatherFromDefauls { [weak self] result in
            switch result {
            case .success(let weather):
                self?.delegate?.updateWeatherObject(weather: weather)
            case .failure:
                break
            }
        }
    }
    
}

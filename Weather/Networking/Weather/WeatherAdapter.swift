import Foundation

class WeatherAdapter {
    
    private let weatherProvider = WeatherProvider()
    
    func downloadWeatherData(latitude: Double, longitude: Double,
                             completion: @escaping (Result<Weather, Error>) -> ()) {
        weatherProvider.downloadWeather(latitude: latitude, longitude: longitude, completion: completion)
    }
}

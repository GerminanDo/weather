//
//  NetworkingManager.swift
//  Weather
//
//  Created by Anton on 1/12/21.
//

import Foundation

class NetworkingManager {
    
    static let weather = WeatherAdapter()
    static let cities = CitiesAdapter()
    
    private init() {}
}

import Foundation

class CitiesProvider {
    
    private let baseURL = "https://places-dsn.algolia.net/1/"
    
    func getCities(query: String, completion: @escaping (Result<Cities, Error>) -> ()) {
        let finalURL = baseURL + "places/query"
        let parameters = ["query": query, "hitsPerPage": 5] as [String : Any]
        guard let url = URL(string: finalURL) else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard let data = data else {return}
            do {
                let cities = try JSONDecoder().decode(Cities.self, from: data)
                DispatchQueue.main.async {
                    completion(.success(cities))
                }
            } catch let error {
                completion(.failure(error))
            }
        }).resume()
    }
}

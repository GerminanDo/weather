import Foundation

class WeatherCache {
    
    private let weather_key = "weather"
    let userDefaults = UserDefaults.standard
    
    func cacheWeather(data: Weather) {
        let encoder = JSONEncoder()
        do {
            let encodedGroup = try encoder.encode(data)
            userDefaults.set(encodedGroup, forKey: weather_key)
        } catch {
            print("Save Failed")
        }
    }
    
    func loadWeatherFromDefauls(completion: @escaping (Result<Weather, Error>) -> ()) {
        let decoder = JSONDecoder()
        do {
            if let data = userDefaults.object(forKey: weather_key) as? Data {
                let weather = try decoder.decode(Weather.self, from: data)
                completion(.success(weather))
            }
        } catch let error {
            completion(.failure(error))
        }
    }

}

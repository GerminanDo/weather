import Foundation

class CitiesAdapter {
    
    private let provider = CitiesProvider()
    
    func getCities(query: String, completion: @escaping (Result<Cities, Error>) -> ()) {
        provider.getCities(query: query, completion: completion)
    }
}

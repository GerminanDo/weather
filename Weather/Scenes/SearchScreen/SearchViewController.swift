import UIKit

protocol SearchViewControllerDelegate: class {
    func searchAnotherPlace(coordinates: GeoLocate)
    func getUserLocationPlace()
}

class SearchViewController: UIViewController {
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cancelButton: UIButton!
       
    private let searchViewModel = SearchViewModel()
    
    weak var delegate: SearchViewControllerDelegate?
    
    var citiesArray: [City] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchViewModel.delegate = self
        cancelButton.addTarget(self, action: #selector(cancelTapped), for: .touchUpInside)
        textField.becomeFirstResponder()
    }
    
    @IBAction func textFieldTextChanged(_ sender: UITextField) {
        if let text = sender.text, !text.isEmpty {
            searchViewModel.getCities(query: text)
        } else {
            citiesArray = []
        }
    }
    
    @IBAction func currentLocationTapped(_ sender: UIButton) {
        delegate?.getUserLocationPlace()
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func cancelTapped(){
        self.dismiss(animated: true, completion: nil)
    }
}

extension SearchViewController: SearchViewModelDelegate {
    func updateTable(with cities: Cities) {
        self.citiesArray = cities.hits
    }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return citiesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CitySearchCell", for: indexPath) as? CitySearchCell else {return UITableViewCell()}
        let cityObj = citiesArray[indexPath.row]
        if let city = cityObj.locale_names.default.first, let administrative = cityObj.administrative.first {
            let coutry = cityObj.country.default
            let text = "\(city), \(administrative), \(coutry)"
            cell.configure(text: text)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let coordinates = citiesArray[indexPath.row]._geoloc
        delegate?.searchAnotherPlace(coordinates: coordinates)
        self.dismiss(animated: true, completion: nil)
    }
}

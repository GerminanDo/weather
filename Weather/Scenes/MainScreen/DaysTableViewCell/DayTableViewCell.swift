import UIKit

class DayTableViewCell: UITableViewCell {
    @IBOutlet weak var dayNameLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var firstTempLabel: UILabel!
    @IBOutlet weak var secondTempLabel: UILabel!
    @IBOutlet weak var precStrengthLabel: UILabel!
    
    func configureView(forecast: Forecast?) {
        guard let forecast = forecast else {return}
        dayNameLabel.text = daysDictionary[forecast.date.getDay()]
        setSvgImageForCell(name: forecast.parts.day_short.icon)
        firstTempLabel.text = forecast.parts.day_short.temp?.description
        secondTempLabel.text = forecast.parts.day_short.temp_min.description
        let precStrength = Int(forecast.parts.day_short.prec_strength * 100)
        if precStrength > 0 {
            precStrengthLabel.text = "\(precStrength.description)%"
        }
    }
    
    private func setSvgImageForCell(name: String) {
        iconImageView.tintColor = .white
        let svgURL = URL(string: "https://yastatic.net/weather/i/icons/blueye/color/svg/\(name).svg")!
        iconImageView.sd_setImage(with: svgURL, completed: {image,_,_,_ in
            self.iconImageView.image = image?.withRenderingMode(.alwaysTemplate)
        })
    }
    
}

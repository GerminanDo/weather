import UIKit

struct ShowHour {
    let hour: String
    let icon: String
    let temp: String
    let strenght: Double
}

class HourCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var strenghtLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!

    private var hoursToShow: [ShowHour] = []
    
    private var sunsetTime = ""
    private var sunsetHour = 0
    private  var sunriseTime = ""
    private  var sunriseHour = 0
    
    func configureCell(weather: Weather?, index: Int, currentHour: Int) {
        guard let weather = weather else {return}
        setSunTimes(weather: weather, currentHour: currentHour)
        configureHoursArray(weather: weather, currentHour: currentHour, complition: { hoursToShow in
            self.hoursToShow = hoursToShow
            insertSunTimes(currentHour: currentHour, complition: {
                let showedHour = self.hoursToShow[index].hour
                let showedTemp = self.hoursToShow[index].temp
                let showedImage = self.hoursToShow[index].icon
                let showedStrenght = self.hoursToShow[index].strenght
                hourLabel.text = index == 0 ? "Сейчас" : showedHour
                tempLabel.text = showedTemp
                strenghtLabel.text = "\(Int(showedStrenght * 100))%"
                strenghtLabel.isHidden = showedStrenght == 0
                if showedImage != "" {
                    setSvgImageForCell(name: showedImage)
                } else {
                    setSimpleImageForCell(showedTemp: showedTemp)
                }
            })
        })
    }
    
    private func setSvgImageForCell(name: String) {
        iconImage.tintColor = .white
        let svgURL = URL(string: "https://yastatic.net/weather/i/icons/blueye/color/svg/\(name).svg")!
        iconImage.sd_setImage(with: svgURL, completed: {image,_,_,_ in
            self.iconImage.image = image?.withRenderingMode(.alwaysTemplate)
        })
    }
    
    private func setSimpleImageForCell(showedTemp: String) {
        if showedTemp == "Восход солнца" {
            iconImage.image = #imageLiteral(resourceName: "sunrise")
        } else {
            iconImage.image = #imageLiteral(resourceName: "sunset")
        }
    }

    private func configureHoursArray(weather: Weather, currentHour: Int, complition: (([ShowHour]) -> ())) {
        var hoursToShow: [ShowHour] = []
        weather.forecasts.first?.hours.forEach({ hourObj in
            if let hour = Int(hourObj.hour), hour >= currentHour {
                hoursToShow.append(ShowHour(hour: hour > 9 ? hour.description : "0\(hour.description)",
                                            icon: hourObj.icon,
                                            temp: "\(hourObj.temp.description)°", strenght: hourObj.prec_strength))
            }
        })
        weather.forecasts[1].hours.forEach { hourObj in
            if let hour = Int(hourObj.hour), hour <= currentHour {
                hoursToShow.append(ShowHour(hour: hour > 9 ? hour.description : "0\(hour.description)", icon: hourObj.icon,
                                            temp: "\(hourObj.temp.description)°", strenght: hourObj.prec_strength))
            }
        }
        complition(hoursToShow)
    }
    
    private func insertSunTimes(currentHour: Int, complition: () -> Void) {
        let sunriseObj = ShowHour(hour: sunriseTime, icon: "", temp: "Восход солнца", strenght: 0)
        let sunsetObj = ShowHour(hour: sunsetTime, icon: "", temp: "Заход солнца", strenght: 0)
        for (index, hour) in hoursToShow.enumerated() {
            let nextIndex = index + 2
            if currentHour != self.sunriseHour {
                if nextIndex < hoursToShow.count, let hour = Int(hour.hour),
                  let nextHour = Int(hoursToShow[nextIndex].hour), self.sunriseHour > hour && self.sunriseHour < nextHour {
                   hoursToShow.insert(sunriseObj, at: nextIndex)
               }
            } else {
                hoursToShow.append(sunriseObj)
            }
        }
        for (index, hour) in hoursToShow.enumerated() {
            let nextIndex = index + 2
            if currentHour != self.sunsetHour {
                if nextIndex < hoursToShow.count, let hour = Int(hour.hour), let nextHour = Int(hoursToShow[nextIndex].hour), self.sunsetHour > hour && self.sunsetHour < nextHour {
                    hoursToShow.insert(sunsetObj, at: nextIndex)
                }
            } else {
                hoursToShow.append(sunsetObj)
            }
        }
        complition()
    }
    
    private func setSunTimes(weather: Weather, currentHour: Int) {
        let sunset = weather.forecasts[0].sunset
        let sunrise = weather.forecasts[0].sunrise
        let nextSunrise = weather.forecasts[1].sunrise
        let nextSunset = weather.forecasts[1].sunset
        
        sunsetTime = currentHour > sunset.getHour() ? nextSunset : sunset
        sunsetHour = sunsetTime.getHour()
        
        sunriseTime = currentHour > sunrise.getHour() ? nextSunrise : sunrise
        sunriseHour = sunriseTime.getHour()
    }
}

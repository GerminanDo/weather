import UIKit

extension Date {
    
    func getHour() -> Int {
        var calendar = Calendar.current

        if let timeZone = TimeZone(identifier: "MSK") {
           calendar.timeZone = timeZone
        }

        return calendar.component(.hour, from: self)
    }
    
    func getMinutes() -> Int {
        var calendar = Calendar.current

        if let timeZone = TimeZone(identifier: "MSK") {
           calendar.timeZone = timeZone
        }

        return calendar.component(.minute, from: self)
    }
    
    func getDay() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "EEEE"
        let str = dateFormatter.string(from: self)
        return str
    }
}

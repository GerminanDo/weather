import Foundation

let conditionDictionary = ["clear" : "Ясно",
                            "partly-cloudy" : "Малооблачно",
                            "cloudy" : "Облачно с прояснениями",
                            "overcast" : "Пасмурно",
                            "drizzle" : "Морось",
                            "light-rain" : "Небольшой дождь",
                            "rain" : "Дождь",
                            "moderate-rain" : "Умеренно сильный дождь",
                            "heavy-rain" : "Сильный дождь",
                            "continuous-heavy-rain" : "Длительный сильный дождь",
                            "showers" : "Ливень",
                            "wet_snow" : "Дождь со снегом",
                            "light-snow" : "Небольшой снег",
                            "snow" : "Снег",
                            "snow-showers" : "Снегопад",
                            "hail" : "Град",
                            "thunderstorm" : "Гроза",
                            "thunderstorm-with-rain" : "Дождь с грозой",
                            "thunderstorm-with-hail" : "Гроза с градом"]

let daysDictionary = ["Monday" : "Понедельник",
                      "Tuesday" : "Вторник",
                      "Wednesday" : "Среда",
                      "Thursday" : "Четверг",
                      "Friday" : "Пятница",
                      "Saturday" : "Суббота",
                      "Sunday" : "Воскресенье"]

let infoDictionary = [0 : "ВОСХОД СОЛНЦА",
                      1 : "ЗАХОД СОЛНЦА",
                      2 : "ВЕРОЯТНОСТЬ ОСАДКОВ",
                      3 : "ВЛАЖНОСТЬ",
                      4 : "ВЕТЕР",
                      5 : "ОЩУЩАЕТСЯ КАК",
                      6 : "ОСАДКИ",
                      7 : "ДАВЛЕНИЕ",
                      8 : "УФ-ИНДЕКС"]

let windDictionary = ["nw" : "сз",
                      "n" : "с",
                      "ne" : "св",
                      "e" : "в",
                      "se" : "юв",
                      "s" : "ю",
                      "sw" : "юг",
                      "w" : "з",
                      "с" : "ш"]
